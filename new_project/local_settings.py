import os

DEBUG = True

SECRET_KEY = 'MY_SECRET_KEY'

STATICFILES_DIRS = [
    os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "static"),
    '~/projects/new_project/static/',
]
