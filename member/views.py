from django.contrib.auth.middleware import AuthenticationMiddleware
from django.views.generic import TemplateView


class MemberCommonView(AuthenticationMiddleware, TemplateView):
    template_name = 'member/templates/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


#TODO: Ajax function for change users
