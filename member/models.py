from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    inn = models.PositiveSmallIntegerField(verbose_name='INN', blank=True, null=True, default=0)
    amount = models.FloatField(verbose_name='Amount of money', default=0)
