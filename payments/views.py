from django.contrib.auth.middleware import AuthenticationMiddleware
from django.http import JsonResponse
from django.views.generic import CreateView, ListView
from django.urls import reverse

from .forms import SendMoneyUsersForm
from member.models import User


class Select2SearchUsers(AuthenticationMiddleware, ListView):
    def get(self, request, *args, **kwargs):
        term = request.GET.get('term', None)
        if term:
            users = list(
                User.objects.filter(
                    inn__icontains = term
                ).exclude(id=request.user.id).values_list('id', 'first_name', 'last_name')
            )
        return JsonResponse({
            'results': [{'id': user[0], 'text': "{} {}".format(user[1], user[2])} for user in users]
        })


class TransferMoneyView(AuthenticationMiddleware, CreateView):
    form_class = SendMoneyUsersForm
    template_name = 'payments/index.html'

    def get_success_url(self):
        return reverse('transfer-money')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        kwargs['user'] = self.request.user

        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context
