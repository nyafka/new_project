from django.db import models

from member.models import User


class MoneyTransfer(models.Model):
    """
        Users transfer money to other users
    """

    sender = models.ForeignKey(
        User, related_name='sender_moneys', on_delete=models.PROTECT, verbose_name='Sender'
    )
    recipients = models.ManyToManyField(
        User, related_name='recipients_money', verbose_name='Recipients'
    )
    amount = models.IntegerField(default=0, verbose_name='Amount money')
    performed = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
