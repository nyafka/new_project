from django import forms
from django.db import transaction

from django_select2.forms import HeavySelect2MultipleWidget

from .models import MoneyTransfer


class SendMoneyUsersForm(forms.ModelForm):
    class Meta:
        model = MoneyTransfer
        fields = [
            'recipients',
            'amount'
        ]
        widgets = {
            'recipients': HeavySelect2MultipleWidget(
                data_url='/payments/select2/users/'
            )
        }

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.user = user

    def clean_recipients(self):
        recipients = self.cleaned_data['recipients']

        if not recipients:
            raise forms.ValidationError("Choice one or more recipients")

        return recipients

    def clean_amount(self):
        amount = self.cleaned_data['amount']

        if self.user.amount < amount:
            raise forms.ValidationError("You don't have enough money")

        return amount

    def save(self):
        f = super().save(commit=False)
        data = self.cleaned_data
        recipients = data['recipients']
        amount = data['amount']
        user = self.user

        with transaction.atomic():
            sum_recipient = float("{:.2f}".format(amount/recipients.count()))
            for recipient in recipients:
                recipient.amount += sum_recipient
                recipient.save()
            user.amount = user.amount - amount
            user.save()

        f.sender = self.user

        return f.save()
