from django.urls import path

from .views import TransferMoneyView, Select2SearchUsers


urlpatterns = [
    path('transfer_money/', TransferMoneyView.as_view(), name='transfer-money'),
    path('select2/users/', Select2SearchUsers.as_view()),
]
