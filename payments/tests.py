from django.test import TestCase


from member.models import User

from .forms import SendMoneyUsersForm


class TestTransferForm(TestCase):
    def setUp(self):
        User.objects.create(username='nyafka', inn=123124, amount=100)
        User.objects.create(username='pit', inn=345345345, amount=50)
        User.objects.create(username='sova', inn=345345345, amount=560)

    def test_transfer_form_not_enough_money(self):
        user = User.objects.get(id=1)
        form_data = {'recipients': User.objects.filter(id__in=[2, 3]), 'amount': 100}
        form = SendMoneyUsersForm(user=user, data=form_data)
        self.assertTrue(form.is_valid())

    def test_transfer_equal_division_money(self):
        user = User.objects.get(id=1)
        form_data = {'recipients': User.objects.filter(id__in=[2, 3]), 'amount': 10}
        form = SendMoneyUsersForm(user=user, data=form_data)
        form.save()
        amount_user = User.objects.get(id=1).amount
        amount_user1 = User.objects.get(id=2).amount
        amount_user2 = User.objects.get(id=3).amount
        self.assertTrue(amount_user1==55)
        self.assertTrue(amount_user2==565)
        self.assertTrue(amount_user==90)
